<?php

session_start();

if(isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] == 1){
	$title = "Admin Products Dashboard";

	function get_content(){
		require "../controllers/connection.php";
		//query the categories
		$queryCategories = "SELECT * FROM categories";
		$categories = mysqli_query($conn, $queryCategories) or die(mysqli_error($conn));

		//query the products
		$queryProducts = "SELECT * FROM products";
		$products = mysqli_query($conn, $queryProducts) or die(mysqli_error($conn)); ?>

		<!-- start of row that will contain an add category collabsible card and an add product collapsible card -->
				<div class="row">
					<div class="col-8 offset-2">
						<div id="accordion">
							<!-- card for add category form -->
							<div class="card">
								<div class="card-header">
									<a class="card-link h3" data-toggle="collapse" href="#addCategory">
										Add Category
									</a>
								</div>

								<div id="addCategory" class="collapse" data-parent="#accordion">
									<div class="card-body">
										<form>
											<div class="form-group">
												<label for="name">Category name: </label>

												<input class="form-control" type="text" id="catName" name="description">
											</div>
										</form>
										<button class="btn btn-success" id="addCatBtn">Add Category</button>
									</div>	
								</div>
							</div>
							<!-- end of add category card -->

							<!-- add product card -->
							<div class="card">
								<div class="card">
									<div class="card-header">
										<a class="card-link h3" data-toggle="collapse" href="#addProduct">
											Add Product
										</a>
									</div>

									<div id="addProduct" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<form method="post" action="../controllers/add_product.php" enctype="multipart/form-data">
												<div class="form-group">
													<label for="name">Name:</label>
													<input class="form-control" type="text" id="name" name="name">
												</div>
												<div class="form-group">
													<label for="description">Description:</label>
													<input class="form-control" type="text" id="description" name="description">
												</div>
												<div class="form-group">
													<label for="price">Price:</label>
													<input class="form-control" type="number" id="price" name="price">
												</div>
												<!-- category select dropdown -->
												<div class="form-group">
													<label for="category_id">Category:</label>
													<select class="form-control" id="category_id" name="category">
														<option>Select a category:</option>
														<?php foreach ($categories as $category) : ?>
															<option value="<?= $category['id']; ?>">
																<?= $category['name']; ?>
															</option>
														<?php endforeach; ?>
													</select>
												</div>
												<div class="form-group">
													<label for="image">Image:</label>
													<input class="form-control" type="file" id="image" name="image">
												</div>
												<button class="btn btn-success">Add Product</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							<!-- end of add product card -->
						</div>
						<!-- end of accordion -->
					</div>
				</div>
				<!-- end of add category and add product row -->
				<script type="text/javascript" src="../assets/js/add_cat.js"></script>
	<?php };

	require "./layouts/app.php";
}else{
	header('location: ./products_catalog.php');
}
?>