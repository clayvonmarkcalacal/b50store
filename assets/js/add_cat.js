//select the elements that we need to work with 
const addCatbtn = document.querySelector('#addCatBtn');
const catInput = document.querySelector('#catName');
const catSelect = document.querySelector('#category_id');

//attach an event listener to the addCatBtn element that will send a fetch request with POST method to the add_category.php controller
addCatBtn.addEventListener("click", ()=>{
	//fetch can process form input as a JS FormData object
	//as such let's instantiate a new FormData object from the FormData class
	let data = new FormData;//at this point, data is empty
	//we use the append() method to 'push' a passed in name-value pair to our FormData object named data
	//accepts 2 arguments:
		//name, value
	data.append('category', catInput.value);

	//give 2 arguments to the fetch() function
		//1.) URL where the request will be sent 
		//2.)an object containing 2 properties:
			//method with value "POST"
			//body containing the FormData object named data instantiated previously
	fetch("../../controllers/add_category.php", {
		method: "POST", 
		body: data
	})
	.then((res)=>{
		return res.text();
	})
	.then((data)=>{
		catSelect.innerHTML += data;
		console.log(data);
	})

})