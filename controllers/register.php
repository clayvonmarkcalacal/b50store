<?php

	require "connection.php";
	//sanitize our form input data via htmlspecialchars() function
	$email = htmlspecialchars($_POST['email']);
	$uname = htmlspecialchars($_POST['username']);
	//hash the password input via the sha1() function
	$password = sha1(htmlspecialchars($_POST['password']));

	//SQL query for adding a new user into the users table
	$sql = "INSERT INTO users (email, username, password) VALUES ('$email', '$uname', '$password')";

	//execute the query via mysqli_query()
	//OR exit and return the encountered connection error if any
	$result = mysqli_query($conn, $sql) or die(mysqli_error($conn));

	if($result){
		header('location: ../views/login.php');
	}