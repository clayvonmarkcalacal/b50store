<?php

	require "connection.php";
	//sanitize our form inputs
	$name = htmlspecialchars($_POST['name']);
	$description = htmlspecialchars($_POST['description']);
	$price = htmlspecialchars($_POST['price']); 
	$category = htmlspecialchars($_POST['category']); 

	/*var_dump($name);
	var_dump($description);
	var_dump($price);
	var_dump($category);
*/

/*var_dump($_FILES['image']);*/
//save the image properties as variables
$filename = $_FILES['image']['name'];
$filesize = $_FILES['image']['size'];
$file_tmpname = $_FILES['image']['tmp_name'];

//get the file extension property of $filename using PHP's pathinfo() function
//PATHINFO_EXTENSION ay constant pag ALL CAPS lahat

//convert the file extension obtained from the previous step into all lower case character's via PHP's strtolower() function
$file_type = strtolower(pathinfo($filename,PATHINFO_EXTENSION));

//declare checkers for validation
$hasdetails = false; 
$isImg = false;

//declare validation conditions that will toggle checker values
if($name != "" && $price >0 && $category != "" && $description !=""){
	$hasdetails = true;
}
if($file_type == "jpg" || $file_type == "jpeg" || $file_type == "png"){
	$isImg = true;
}
if($filesize > 0 && $isImg && $hasdetails){
	//we can now proceed with the file upload
	$final_filepath = "../assets/images/".$filename;
	//move the image from its temporary location to its final destination via PHP's move_uploaded_file() function
	move_uploaded_file($file_tmpname, $final_filepath);
}else {
	echo "Please upload an image.";
}
$query = "INSERT INTO products(name, description, price, category_id, img_path) VALUES ('$name', '$description', '$price', '$category', '$final_filepath')";
mysqli_query($conn, $query) or die (mysqli_error($conn));

//$_SERVER['HTTP_REFERER'] denots the path leading to where the HTTP request that was recieved came from
header('location: '. $_SERVER['HTTP_REFERER']);